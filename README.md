# README #

Contains code and notes from studying [GraphQL & Flutter - Build GraphQL APIs & Flutter Client Apps](https://www.udemy.com/course/graphql_flutter_build_apis/).

## Run the code ##

Get node packages:

```bash
npm install
```

Run in dev mode:

```bash
npm run dev
```

Run in production mode:

```bash
npm start
```

## Deployment ##

The GraphQL server app is deployed on Heroku as "graphql-flutter-course-1".

Login to Heroku and set up:

```bash
heroku login
heroku git:remote -a graphql-flutter-course-1
```

Define env variables to Heroku use [heroku config](https://devcenter.heroku.com/articles/config-vars)
, or use the settings tba in the web dashboard.

Deploy to Heroku:

```bash
git push heroku master
```

Check for errors:

```bash
heroku logs --tail
```

## Access to Heroku ##

Browse to [the deployed app](https://graphql-flutter-course-1.herokuapp.com/graphql).

Alternatively, use [GraphQL Playground](https://www.electronjs.org/apps/graphql-playground) and paste into it the Heroku url from above.

## Links ##

[GraphQL cheat sheet](https://wehavefaces.net/graphql-shorthand-notation-cheatsheet-17cd715861b6)
