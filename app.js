const express = require('express')
const { graphqlHTTP } = require('express-graphql')
const mongoose = require('mongoose')
const cors = require('cors')

const schema = require('./graphql-server/schema/schema')
const testSchema = require('./graphql-server/schema/types_schema')

const app = express()
app.use(cors())

const port = process.env.PORT || 4000

app.use('/graphql', graphqlHTTP({
  graphiql: true,
  schema: schema
}))

mongoose.connect(`mongodb+srv://${process.env.MONGODB_USER_NAME}:${process.env.MONGODB_USER_PASSWORD}@${process.env.MONGODB_SERVER_NAME}/${process.env.MONGODB_DB_NAME}?retryWrites=true&w=majority`)
.then(() => {
  app.listen({port}, () => {
    console.log(`Listening to port ${port}`)
  })
}).catch((err) => {
  console.error(err)
})

