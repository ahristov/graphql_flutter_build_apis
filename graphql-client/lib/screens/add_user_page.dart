import 'package:flutter/material.dart';
import 'package:graphgl_client/stylings/stylings.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'home_screen.dart';

class AddUserPage extends StatefulWidget {
  const AddUserPage({Key? key}) : super(key: key);

  @override
  _AddUserPageState createState() => _AddUserPageState();
}

class _AddUserPageState extends State<AddUserPage> {
  final _formKey = GlobalKey<FormState>();
  final _hobbyFormKey = GlobalKey<FormState>();
  final _postFormKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();
  final _ageController = TextEditingController();
  final _professionController = TextEditingController();
  final _hobbyTitleController = TextEditingController();
  final _hobbyDescriptionController = TextEditingController();
  final _postCommentController = TextEditingController();

  bool _isSaving = false;
  bool _visible = false;
  bool _isSavingHobby = false;
  bool _isSavingPost = false;

  String? currUserId;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Add a user',
          style: TextStyle(
            color: Colors.grey,
            fontSize: 19,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.lightGreen,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(20),
          margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 6),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
            boxShadow: [
              BoxShadow(
                offset: const Offset(0, 10),
                color: Colors.grey.shade300,
                blurRadius: 30,
              ),
            ],
          ),
          child: Column(
            children: [
              Mutation(
                options: MutationOptions(
                  document: gql(
                    insertUser(),
                  ),
                  fetchPolicy: FetchPolicy.noCache,
                  onCompleted: (data) {
                    print(
                        data); // {__typename: Mutation, createUser: {__typename: User, id: 61c10af702fcb2e10a71ef60, name: Bingo, profession: bongo, age: 12}}
                    setState(() {
                      currUserId = data['createUser']['id'];
                      _isSaving = false;
                      _visible = true;
                    });
                  },
                ),
                builder: (runMutation, result) {
                  return Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        const SizedBox(height: 12),
                        TextFormField(
                          controller: _nameController,
                          decoration: const InputDecoration(
                            labelText: 'Name',
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide(),
                            ),
                          ),
                          validator: (value) => (value!.isEmpty) ? 'Name cannot be empty' : null,
                          keyboardType: TextInputType.text,
                        ),
                        const SizedBox(height: 12),
                        TextFormField(
                          controller: _professionController,
                          decoration: const InputDecoration(
                            labelText: 'Profession',
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide(),
                            ),
                          ),
                          validator: (value) => (value!.isEmpty) ? 'Profession cannot be empty' : null,
                          keyboardType: TextInputType.text,
                        ),
                        const SizedBox(height: 12),
                        TextFormField(
                          controller: _ageController,
                          decoration: const InputDecoration(
                            labelText: 'Age',
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderSide: BorderSide(),
                            ),
                          ),
                          validator: (value) => (value!.isEmpty) ? 'Age cannot be empty' : null,
                          keyboardType: TextInputType.number,
                        ),
                        const SizedBox(height: 12),
                        _isSaving
                            ? const SizedBox(
                                height: 20,
                                width: 20,
                                child: CircularProgressIndicator(
                                  strokeWidth: 3,
                                ))
                            : TextButton(
                                style: buildButtonStyle(),
                                child: const Padding(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 36,
                                    vertical: 12,
                                  ),
                                  child: Text('Save'),
                                ),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    setState(() {
                                      _isSaving = true;
                                    });
                                    runMutation({
                                      'name': _nameController.text.trim(),
                                      'profession': _professionController.text.trim(),
                                      'age': int.parse(_ageController.text.trim()),
                                    });
                                  }
                                },
                              ),
                      ],
                    ),
                  );
                },
              ),

              // Add Hobby
              Visibility(
                visible: _visible,
                child: Mutation(
                  options: MutationOptions(
                    document: gql(insertHobby()),
                    fetchPolicy: FetchPolicy.noCache,
                    onCompleted: (data) {
                      print(
                          data); // {__typename: Mutation, createHobby: {__typename: Hobby, id: 61c113d902fcb2e10a71ef6a, title: Blah}}
                      setState(() {
                        _isSavingHobby = false;
                      });
                    },
                  ),
                  builder: (runMutation, result) {
                    return Form(
                      key: _hobbyFormKey,
                      child: Column(
                        children: [
                          const SizedBox(height: 12),
                          TextFormField(
                            controller: _hobbyTitleController,
                            decoration: const InputDecoration(
                              labelText: 'Hobby title',
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(),
                              ),
                            ),
                            validator: (value) => (value!.isEmpty) ? 'Hobby title cannot be empty' : null,
                            keyboardType: TextInputType.text,
                          ),
                          const SizedBox(height: 12),
                          TextFormField(
                            controller: _hobbyDescriptionController,
                            decoration: const InputDecoration(
                              labelText: 'Hobby description',
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(),
                              ),
                            ),
                            validator: (value) => (value!.isEmpty) ? 'Hobby description cannot be empty' : null,
                            keyboardType: TextInputType.text,
                          ),
                          const SizedBox(height: 12),
                          _isSavingHobby
                              ? const SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 3,
                                  ))
                              : TextButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(Colors.greenAccent),
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 36,
                                      vertical: 12,
                                    ),
                                    child: Text('Save'),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      setState(() {
                                        _isSavingHobby = true;
                                      });
                                      runMutation({
                                        'title': _hobbyTitleController.text.trim(),
                                        'description': _hobbyDescriptionController.text.trim(),
                                        'userId': currUserId,
                                      });
                                    }
                                  },
                                ),
                        ],
                      ),
                    );
                  },
                ),
              ),

              // Add Post
              Visibility(
                visible: _visible,
                child: Mutation(
                  options: MutationOptions(
                    document: gql(insertPost()),
                    fetchPolicy: FetchPolicy.noCache,
                    onCompleted: (data) {
                      print(
                          data); // {__typename: Mutation, createPost: {__typename: Post, id: 61c118b102fcb2e10a71ef8e, comment: Blah}}
                      setState(() {
                        _isSavingPost = false;
                      });
                    },
                  ),
                  builder: (runMutation, result) {
                    return Form(
                      key: _postFormKey,
                      child: Column(
                        children: [
                          const SizedBox(height: 12),
                          TextFormField(
                            controller: _postCommentController,
                            decoration: const InputDecoration(
                              labelText: 'Post content',
                              fillColor: Colors.white,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(),
                              ),
                            ),
                            validator: (value) => (value!.isEmpty) ? 'Post content cannot be empty' : null,
                            keyboardType: TextInputType.text,
                          ),
                          const SizedBox(height: 12),
                          _isSavingPost
                              ? const SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    strokeWidth: 3,
                                  ))
                              : TextButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(Colors.greenAccent),
                                  ),
                                  child: const Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 36,
                                      vertical: 12,
                                    ),
                                    child: Text('Save'),
                                  ),
                                  onPressed: () {
                                    if (_formKey.currentState!.validate()) {
                                      setState(() {
                                        _isSavingPost = true;
                                      });
                                      runMutation({
                                        'comment': _postCommentController.text.trim(),
                                        'userId': currUserId,
                                      });
                                    }
                                  },
                                ),
                        ],
                      ),
                    );
                  },
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Visibility(
                    visible: _visible,
                    child: TextButton(
                      child: const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 26.0, vertical: 12.0),
                        child: Text("Done", style: TextStyle(color: Colors.grey, fontSize: 16)),
                      ),
                      style: buildButtonStyle(),
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
                          builder: (context) {
                            return const HomeScreen();
                          },
                        ), (route) => false);
                      },
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  String insertUser() {
    return '''
mutation createUser(\$name: String!, \$age: Int!, \$profession: String!) {
  createUser(name: \$name, age: \$age, profession: \$profession) {
    id
    name
  }
}
    ''';
  }

  String insertHobby() {
    return '''
mutation createHobby(\$title: String!, \$description: String!, \$userId: ID!) {
  createHobby(title: \$title, description: \$description, userId: \$userId) {
    id
    title
  }
}
    ''';
  }

  String insertPost() {
    return '''
mutation createPost(\$comment: String!, \$userId: ID!) {
  createPost(comment: \$comment, userId: \$userId) {
    id
    comment
  }
}
    ''';
  }
}
