import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:graphgl_client/stylings/stylings.dart';

class DetailsPage extends StatefulWidget {
  final dynamic user;
  const DetailsPage({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  bool _showHobbies = true;
  bool _showPosts = false;

  void toggleView(bool showHobbies) {
    setState(() {
      _showHobbies = showHobbies;
      _showPosts = !showHobbies;
    });
  }

  List<Widget> _listOfHobies() {
    List<dynamic> hobbies = widget.user['hobbies'] ?? [];
    return hobbies
        .map((e) => _ContentCard(
              title: 'Hobby: ${e['title'] ?? 'N/A'}',
              texts: [
                'Description: ${e['description'] ?? 'N/A'}',
                'Author: ${e['user']['name']}',
              ],
            ))
        .toList();
  }

  List<Widget> _listOfPosts() {
    List<dynamic> posts = widget.user['posts'] ?? [];
    return posts
        .map((e) => _ContentCard(
              title: 'Post comment: ${e['comment'] ?? 'N/A'}',
              texts: [
                'Author: ${e['user']['name']}',
              ],
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(
          widget.user['name'],
          style: const TextStyle(
            color: Colors.grey,
            fontWeight: FontWeight.bold,
            fontSize: 19,
          ),
        ),
      ),
      body: Column(
        children: [
          _ContentCard(
            title: (widget.user['name'] ?? 'N/A').toString().toUpperCase(),
            texts: [
              'Occupation: ${widget.user['profession']}',
              'Age: ${widget.user['age']}',
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _Button(onPressed: () => toggleView(true), text: 'Hobbies'),
                _Button(onPressed: () => toggleView(false), text: 'Posts'),
              ],
            ),
          ),
          if (_showHobbies) ..._listOfHobies(), // Or can wrap in Visibility() + ListView
          if (_showPosts) ..._listOfPosts(),
        ],
      ),
    );
  }
}

class _ContentCard extends StatelessWidget {
  final String title;
  final List<String> texts;

  const _ContentCard({
    Key? key,
    required this.title,
    required this.texts,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 6),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            offset: const Offset(0, 10),
            color: Colors.grey.shade300,
            blurRadius: 30,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
          ...texts.map((text) => Padding(
                padding: const EdgeInsets.only(top: 10, left: 8),
                child: Text(text),
              )),
        ],
      ),
    );
  }
}

class _Button extends StatelessWidget {
  final String text;
  final Function()? onPressed;

  const _Button({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 12),
        child: Text(
          text,
          style: const TextStyle(color: Colors.grey, fontSize: 16),
        ),
      ),
      style: buildButtonStyle(),
    );
  }
}
