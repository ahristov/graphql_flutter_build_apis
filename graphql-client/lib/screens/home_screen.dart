import 'package:flutter/material.dart';

import 'add_user_page.dart';
import 'users_page.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Users & Hobbies",
          style: TextStyle(
            color: Colors.grey,
            fontSize: 19,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: const Center(
        child: UsersPage(),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.group_add),
        backgroundColor: Colors.lightGreen,
        onPressed: () async {
          final route = MaterialPageRoute(builder: (context) => const AddUserPage());
          await Navigator.push(context, route);
        },
      ),
    );
  }
}
