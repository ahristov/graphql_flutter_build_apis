import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'details_page.dart';
import 'home_screen.dart';
import 'update_user_page.dart';

class UsersPage extends StatefulWidget {
  const UsersPage({Key? key}) : super(key: key);

  @override
  _UsersPageState createState() => _UsersPageState();
}

class _UsersPageState extends State<UsersPage> {
  final users = [];
  final _query = '''
query {
  users {
    id
    name
    age
    profession
    hobbies {
      id
      title
      description
      userId
      user {
        name
      }
    }
    posts {
      id
      comment
      userId
      user {
        name
      }
    }
  }
}
  ''';

  List hobbyIDsToDelete = [];
  List postIDsToDelete = [];

  bool _isRemoveHobbies = false;
  bool _isRemovePosts = false;

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(document: gql(_query)),
      builder: (result, {fetchMore, refetch}) {
        if (result.isLoading) {
          return const CircularProgressIndicator();
        }

        final users = result.data!['users'];

        return (users.isNotEmpty)
            ? ListView.builder(
                itemCount: users.length,
                itemBuilder: (context, index) {
                  final user = users[index];
                  return Stack(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(bottom: 23, left: 10, right: 10),
                        padding: const EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                              offset: const Offset(0, 10),
                              color: Colors.grey.shade300,
                              blurRadius: 30,
                            ),
                          ],
                        ),
                        child: InkWell(
                          onTap: () async {
                            final route = MaterialPageRoute(
                              builder: (context) {
                                return DetailsPage(user: user);
                              },
                            );
                            await Navigator.push(context, route);
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    '${user['name']}',
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      InkWell(
                                        child: const Icon(Icons.edit, color: Colors.greenAccent),
                                        onTap: () async {
                                          final route = MaterialPageRoute(
                                            builder: (context) {
                                              return UpdateUser(
                                                id: user['id'],
                                                name: user['name'],
                                                age: user['age'],
                                                profession: user['profession'],
                                              );
                                            },
                                          );
                                          await Navigator.push(context, route);
                                        },
                                      ),
                                      Mutation(
                                        options: MutationOptions(
                                          document: gql(removeUser()),
                                          fetchPolicy: FetchPolicy.noCache,
                                          onCompleted: (data) {
                                            print(
                                                data); // {__typename: Mutation, removeUser: {__typename: User, name: Blah}}
                                          },
                                        ),
                                        builder: (runMutation, result) {
                                          return Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: InkWell(
                                              child: const Icon(Icons.delete_forever, color: Colors.red),
                                              onTap: () async {
                                                hobbyIDsToDelete.clear();
                                                postIDsToDelete.clear();
                                                for (int i = 0; i < user['hobbies'].length; i++) {
                                                  hobbyIDsToDelete.add(user['hobbies'][i]['id']);
                                                }
                                                for (int i = 0; i < user['posts'].length; i++) {
                                                  postIDsToDelete.add(user['posts'][i]['id']);
                                                }
                                                setState(() {
                                                  _isRemoveHobbies = true;
                                                  _isRemovePosts = true;
                                                });

                                                runMutation({'id': user['id']});
                                                Navigator.pushAndRemoveUntil(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) => const HomeScreen(),
                                                    ),
                                                    (route) => false);
                                              },
                                            ),
                                          );
                                        },
                                      ),
                                      _isRemoveHobbies
                                          ? Mutation(
                                              options: MutationOptions(
                                                document: gql(removeHobbies()),
                                                onCompleted: (data) {
                                                  print(data);
                                                },
                                              ),
                                              builder: (runMutation, result) {
                                                if (hobbyIDsToDelete.isNotEmpty) {
                                                  runMutation({'ids': hobbyIDsToDelete});
                                                }
                                                return const SizedBox.shrink();
                                              },
                                            )
                                          : const SizedBox.shrink(),
                                      _isRemovePosts
                                          ? Mutation(
                                              options: MutationOptions(
                                                document: gql(removePosts()),
                                                onCompleted: (data) {
                                                  print(data);
                                                },
                                              ),
                                              builder: (runMutation, result) {
                                                if (postIDsToDelete.isNotEmpty) {
                                                  runMutation({'ids': postIDsToDelete});
                                                }
                                                return const SizedBox.shrink();
                                              },
                                            )
                                          : const SizedBox.shrink(),
                                    ],
                                  )
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                                child: Text(
                                  'Occupation: ${user['profession'] ?? 'N/A'}',
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                                child: Text(
                                  'Age: ${user['age'] ?? 'N/A'}',
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                },
              )
            : const Center(
                child: Text('No users found'),
              );
      },
    );
  }

  String removeUser() {
    return '''
mutation removeUser(\$id: ID!) {
  removeUser(id: \$id){
      name
      posts {
        id
      }
      hobbies {
        id
      }
  }
}
    ''';
  }

  String removeHobbies() {
    return '''
mutation removeHobbies(\$ids: [ID!]!) {
  removeHobbies(ids: \$ids) {
    id
  }
}
    ''';
  }

  String removePosts() {
    return '''
mutation removePosts(\$ids: [ID!]!) {
  removePosts(ids: \$ids) {
    id
  }
}
    ''';
  }
}
