const graphql = require('graphql')
const _ = require('lodash')

const User = require('../model/user')
const Hobby = require('../model/hobby')
const Post = require('../model/post')

// Dummy data

// var userData = [
//   {id: '1', name: 'Bond', age: 36, profession: 'Programmer'},
//   {id: '13', name: 'Anna', age: 26, profession: 'Baker'},
//   {id: '211', name: 'Bella', age: 16, profession: 'Mechanic'},
//   {id: '19', name: 'Gina', age: 26, profession: 'Painter'},
//   {id: '150', name: 'Georgina', age: 36, profession: 'Teacher'}
// ]

// var hobbyData = [
//   {id: '1', title: 'Programming', description: 'Using computers to make the world a better place', userId: '1'},
//   {id: '2', title: 'Rowing', description: 'Sweat and feel better before eating donuts', userId: '1'},
//   {id: '3', title: 'Swimming', description: 'Get in the water and learn to become the water', userId: '211'},
//   {id: '4', title: 'Fencing', description: 'Hobby for fencing people', userId: '211'},
//   {id: '5', title: 'Hiking', description: 'Hiking boots and explore the world', userId: '150'},
// ]

// var postData = [
//   {id: '1', comment: 'Building a Mind', userId: '1'},
//   {id: '2', comment: 'GraphQL is Amazing', userId: '1'},
//   {id: '3', comment: 'How to Change the World', userId: '19'},
//   {id: '4', comment: 'How to Change the Universe', userId: '211'},
//   {id: '5', comment: 'How to Change the People', userId: '1'}
// ]

const {
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLSchema
} = graphql

// Types

const UserType = new GraphQLObjectType({
  name: 'User',
  description: 'User description comes here...',
  fields: () => ({
    id: {type: GraphQLID},
    name: {type: GraphQLString},
    age: {type: GraphQLInt},
    profession: {type: GraphQLString},
    posts: {
      type: new GraphQLList(PostType),
      resolve(parent, args) {
        // return _.filter(postData, {userId: parent.id})
        return Post.find({userId: parent.id})
      }
    },
    hobbies: {
      type: new GraphQLList(HobbyType),
      resolve(parent, args) {
        // return _.filter(hobbyData, {userId: parent.id})
        return Hobby.find({userId: parent.id})
      }
    },
  })
})

const HobbyType = new GraphQLObjectType({
  name: 'Hobby',
  description: 'Hobby description comes here...',
  fields: () => ({
    id: {type: GraphQLID},
    title: {type: GraphQLString},
    description: {type: GraphQLString},
    userId: {type: GraphQLID},
    user: {
      type: UserType,
      resolve(parent, args) {
        // return _.find(userData, {id: parent.userId})
        return User.findById(parent.userId)
      }
    }
  })
})

const PostType = new GraphQLObjectType({
  name: 'Post',
  description: 'Post description comes here...',
  fields: () => ({
    id: {type: GraphQLID},
    comment: {type: GraphQLString},
    userId: {type: GraphQLID},
    user: {
      type: UserType,
      resolve(parent, args) {
        // return _.find(userData, {id: parent.userId})
        return User.findById(parent.userId)
      }
    }
  })
})

// Queries

const RootQueryType = new GraphQLObjectType({
  name: 'RootQuery',
  description: 'Description of root query comes here...',
  fields: {
    user: {
      type: UserType,
      args: {id: {type: GraphQLID}},
      resolve(parent, args) {
        // return _.find(userData, {id: args.id})
        return User.findById(args.id)
      }
    },
    users: {
      type: new GraphQLList(UserType),
      resolve(parent, args) {
        // return userData
        return User.find();
      }
    },

    hobby: {
      type: HobbyType,
      args: {id: {type: GraphQLID}},
      resolve(parent, args) {
        // return _.find(hobbyData, {id: args.id})
        return Hobby.findById(args.id)
      }
    },
    hobbies: {
      type: new GraphQLList(HobbyType),
      resolve(parent, args) {
        // return hobbyData
        return Hobby.find()
      }
    },

    post: {
      type: PostType,
      args: {id: {type: GraphQLID}},
      resolve(parent, args) {
        // return _.find(postData, {id: args.id})
        return Post.findById(args.id)
      }
    },
    posts: {
      type: new GraphQLList(PostType),
      resolve(parent, args) {
        // return postData
        return Post.find()
      }
    }
  }
})

// Mutations

const MutationType = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Description of mutation comes here...',
  fields: {
    createUser: {
      type: UserType,
      args: {
        name: {type: new GraphQLNonNull(GraphQLString)},
        age: {type: new GraphQLNonNull(GraphQLInt)},
        profession: {type: GraphQLString}
      },
      resolve(parent, {name, age, profession}) {
        return User({
          name,
          age,
          profession
        }).save()
      }
    },
    updateUser: {
      type: UserType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        name: {type: new GraphQLNonNull(GraphQLString)},
        age: {type: new GraphQLNonNull(GraphQLInt)},
        profession: {type: GraphQLString}
      },
      resolve(parent, {id, name, age, profession}) {
        return User.findByIdAndUpdate(id, {
          $set: {
            name,
            age,
            profession
          }
        }, {new: true}) // send back the updated object type
      }
    },
    removeUser: {
      type: UserType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
      },
      resolve(parent, {id}) {
        const removedUser = User.findByIdAndRemove(id).exec()
        if (!removedUser) {
          throw new Error('Error removing user')
        }
        return removedUser
      }
    },


    createPost: {
      type: PostType,
      args: {
        comment: {type: new GraphQLNonNull(GraphQLString)},
        userId: {type: new GraphQLNonNull(GraphQLID)}
      },
      resolve(parent, {comment, userId}) {
        const post = Post({
          comment,
          userId
        })
        return post.save()
      }
    },
    updatePost: {
      type: PostType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        comment: {type: new GraphQLNonNull(GraphQLString)},
      },
      resolve(parent, {id, comment}) {
        return Post.findByIdAndUpdate(id, {
          $set: {
            comment
          }
        }, {new: true})
      }
    },
    removePost: {
      type: PostType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
      },
      resolve(parent, {id}) {
        const removedPost = Post.findByIdAndRemove(id).exec()
        if (!removedPost) {
          throw new Error('Error removing post')
        }
        return removedPost
      }
    },
    removePosts: {
      type: PostType,
      args: {
        ids: {type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(GraphQLID)))},
      },
      resolve(parent, {ids}) {
        const removedPosts = Post.deleteMany({
          _id: ids
        }).exec()
        if (!removedPosts) {
          throw new Error('Error removing posts')
        }
        return removedPosts
      }
    },

    createHobby: {
      type: HobbyType,
      args: {
        title: {type: new GraphQLNonNull(GraphQLString)},
        description: {type: new GraphQLNonNull(GraphQLString)},
        userId: {type: new GraphQLNonNull(GraphQLID)}
      },
      resolve(parent, {title, description, userId}) {
        const hobby = Hobby({
          title,
          description,
          userId
        })
        return hobby.save();
      }
    },
    updateHobby: {
      type: HobbyType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
        title: {type: new GraphQLNonNull(GraphQLString)},
        description: {type: new GraphQLNonNull(GraphQLString)}
      },
      resolve(parent, {id, title, description}) {
        return Hobby.findByIdAndUpdate(id, {
          $set: {
            title,
            description
          }
        }, {new: true})
      }
    },
    removeHobby: {
      type: HobbyType,
      args: {
        id: {type: new GraphQLNonNull(GraphQLID)},
      },
      resolve(parent, {id}) {
        const removedHobby = Hobby.findByIdAndRemove(id).exec()
        if (!removedHobby) {
          throw new Error('Error removing hobby')
        }
        return removedHobby
      }
    },
    removeHobbies: {
      type: HobbyType,
      args: {
        ids: {type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(GraphQLID)))},
      },
      resolve(parent, {ids}) {
        const removedHobbies = Hobby.deleteMany({
          _id: ids
        }).exec()
        if (!removedHobbies) {
          throw new Error('Error removing hobbies')
        }
        return removedHobbies
      }
    },
  }
})

module.exports = new GraphQLSchema({
  query: RootQueryType,
  mutation: MutationType
})