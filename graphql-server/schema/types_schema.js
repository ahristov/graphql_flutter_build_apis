const graphql = require('graphql')

const {
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLNonNull,
  GraphQLList,
  GraphQLObjectType,
  GraphQLSchema
} = graphql

const PersonType = new GraphQLObjectType({
  name: 'Person',
  description: 'Person description comes here ...',
  fields: () => ({
    // Scalar types
    id: {type: GraphQLID}, // ID
    name: {type: new GraphQLNonNull(GraphQLString)}, // String, Non nullable
    age: {type: new GraphQLNonNull(GraphQLInt)}, // Int
    isMarried: {type: GraphQLBoolean}, // Boolean
    gpa: {type: GraphQLFloat}, // Float

    // Object type
    justAType: {
      type: PersonType,
      resolve(parent, args) {
        return parent
      }
    }
  })
})



// RootQuery
const RootQueryType = new GraphQLObjectType({
  name: 'RootQuery',
  description: 'Description for RootQuery here...',
  fields: {
    person: {
      type: PersonType,
      resolve(parent, args) {
        let person = {
          name: 'Tony',
          age: 51,
          isMarried: false,
          gpa: 4.0
        }
        return person
      }
    }
  }
})

module.exports = new GraphQLSchema({
  query: RootQueryType
})